module Fable.ReactNative.Sound

open Fable.Core

type Sound =
    abstract isLoaded: unit -> bool
    abstract play: ?callBack: (bool -> unit) -> unit
    abstract stop: ?callBack: (unit -> unit) -> unit
    abstract setVolume: float -> unit

type RNSound =
    [<Emit("new $0($1)")>]
    abstract createSound: path: string -> Sound

[<ImportDefault("react-native-sound")>]
let Sound: RNSound = jsNative