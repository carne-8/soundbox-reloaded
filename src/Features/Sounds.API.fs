module API.Sounds

open Fable.Core
open Thoth.Fetch

type Sound =
    { name: string
      soundUri: string
      imageUri: string }

type ApiResult =
    { sounds: Sound [] }

[<Import("SOUND_URL", "@env")>]
let soundUrl: string = jsNative

let fetchSounds () : JS.Promise<Result<Sound [], FetchError>> =
    Fetch.tryGet<obj, ApiResult> soundUrl
    |> Promise.mapResult (fun response -> response.sounds)