namespace App

[<RequireQualifiedAccess>]
type Page =
    | Loading

[<RequireQualifiedAccess>]
type Msg =
    | SoundsUpdated of obj []
    | SoundsCharged of SoundManager.Sound []

    | PlaySound of SoundManager.Sound
    | SoundFinished of SoundManager.Sound

type Model =
    { Sounds: SoundManager.Sound [] Loading }