module App.View

open Feliz
open Feliz.ReactNative

let renderSoundComponent onPress (item: FlatListItem<SoundManager.Sound>) =
    Components.Sound.View
        {| Sound = item.item
           OnPress = onPress |}

let View model dispatch =
    let colors = Colors.getColors false

    match model.Sounds with
    | Loading (Some sounds)
    | Loaded sounds ->
        Comp.view [
            prop.style [
                style.paddingHorizontal 7.5
                style.backgroundColor colors.Background
                style.flexDirection.column
                style.flex 1
            ]
            prop.children [
                Comp.text [
                    prop.style [
                        style.fontSize 50.
                        style.fontFamily "ProductSans-Regular"
                        style.color colors.OnBackground

                        style.paddingLeft 7.5
                        style.marginBottom 30.

                        style.flex 1
                        style.textAlign.left
                        style.textAlignVertical.bottom
                    ]
                    prop.text "Sounds"
                ]

                Comp.view [
                    prop.style [
                        style.flex 2.5
                    ]
                    prop.children [
                        Comp.flatList [
                            prop.data sounds
                            prop.renderItem (renderSoundComponent (Msg.PlaySound >> dispatch))
                            prop.keyExtractor (fun (sound: SoundManager.Sound, _) -> sound.Image)
                            prop.numColumns 3
                        ]
                    ]
                ]
            ]
        ]
    | _ -> React.fragment []